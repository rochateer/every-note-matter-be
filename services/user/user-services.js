const { dbconn } = require("../../config/db/db.config");
const { uuidGenerator } = require("../../utils/functionalMethods");

const getAllUser = () => {
  return new Promise((resolve, reject) => {
    dbconn.query(`SELECT * FROM "accounts_enm"`, (error, results) => {
      if(error){
        reject(error);
      }
      if(!results){
        reject('undefined results')
      }
      resolve(results.rows);
    });
  });
}

const getUserByEmail = (email) => {
  return new Promise((resolve, reject) => {
    dbconn.query(`SELECT * FROM "accounts_enm" WHERE email = '${email}'`, (error, results) => {
      if(error){
        reject(error);
      }
      if(!results){
        reject('undefined results')
      }
      resolve(results.rows);
    });
  });
}

const createUserDb = (data) => {
  const { user_uuid, user_name, email, sso_id, provider } = data

  const current_date_time = new Date();

  return new Promise((resolve, reject) => {
    dbconn.query(`INSERT INTO accounts_enm (user_uuid, user_name, email, password, sso_id, provider, created_at, updated_at, last_login) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`, 
      [user_uuid, user_name, email, sso_id, sso_id, provider, current_date_time, current_date_time, current_date_time],
      (error, results) => {
        if(error){
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
}

const updateUserDb = (data, email_condition) => {
  let field_name = [];
  let field_value = [];

  let index = 1
  for (const field in object) {
    field_name.push(field+' = $'+index);
    field_value.push(object[field]);
    index = index+1;
  }

  return new Promise((resolve, reject) => {
    dbconn.query(`UPDATE accounts_enm SET (${field_name.join(",")}) WHERE email = '${email_condition}'`, 
      [...field_value],
      (error, results) => {
        if(error){
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
}

const createUserPreprocessing = (data) => {
  let user_data = {...data};

  user_data["user_uuid"] = uuidGenerator();

  return user_data;
}

module.exports = {
  getAllUser,
  getUserByEmail,
  createUserDb,
  updateUserDb,
  createUserPreprocessing
}