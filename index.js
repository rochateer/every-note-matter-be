const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const debe = require("./config/db/db.config");
const {createSpreadsheet} = require("./controller/spreadsheet/spreadsheet-controller");
const { getUsers } = require("./controller/user-management/user-controller");

const authRouter = require("./router/auth-router")

const app = express()

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended : true}));

app.get("/", (req, res) => {
  res.json({message : "Welcome to Every Note Matter backend"})
})

app.get('/get-users', getUsers)

app.post('/create-spreadsheet', createSpreadsheet)

app.use('/', authRouter);

const PORT = process.env.PORT || 3005;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})