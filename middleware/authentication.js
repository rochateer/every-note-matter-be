const authenticateGoogle = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[0];
  if(!token){
    return res.sendStatus(401);
  }
  next();
}

exports.module ={
  authenticateGoogle
}