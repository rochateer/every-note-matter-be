const uuidGenerator = () => {
  let origin = 'xxxxxxxxxxxxxxxxxxxxxxxxx';
  let uuid  = origin.replace(/[x]/g, function(x) {
    const rand = Math.random() * 15 | 0
    const nv = x === 'x' ? rand : x
    return rand.toString(16)
  })
  return uuid
}

module.exports = {
  uuidGenerator
}