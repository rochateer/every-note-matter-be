// utils/googleSheets.ts

const { google, sheets_v4 } = require("googleapis");

// import { google, sheets_v4 } from 'googleapis';


// Function to create a spreadsheet and fill it with data
async function createSpreadsheet(req, res){

  // Extract the access token from the session
  // const { accessToken } = req.data;

  const accessToken = 'ya29.a0Ad52N39Er-FLJ-mYoOiPH65Io8NmJ-M4txrHkaxFOR8RcbLgm67iE7zSuVJ4AdnukT2-pBA6hy0WLSkkno5eu67Y-Xhe24m2mEimnVjcVevGulGLkcruSMNSJZb5trz_pQ04IUNbb676TrttUJCiPYnREFFJOEqc1-6KaCgYKAcASARISFQHGX2MirR5Q3rW9rQNGvA2ZnHShkg0171'

  // Set up OAuth 2.0 authentication using the access token
  const auth = new google.auth.OAuth2();
  auth.setCredentials({ access_token: accessToken });

  // Set the appropriate scopes for accessing Google Sheets
  const sheets = google.sheets({ version: 'v4', auth });

  try {
    // Create a new spreadsheet
    const spreadsheetResponse = await sheets.spreadsheets.create({
      requestBody: {
        properties: {
          title: 'My Spreadsheet',
        },
      },
    });

    // Access spreadsheetId correctly
    const spreadsheetId = spreadsheetResponse.data.spreadsheetId;

    if(!spreadsheetId){
      throw new Error('There is something error when create spreadsheet'); 
    }

    // Define data to be added to the spreadsheet
    const values = [
      {
        range: 'Sheet1!A1:B3',
        values: [
          ['Name', 'Age'],
          ['John', 30],
          ['Alice', 25],
        ],
      },
    ];

    // Fill the spreadsheet with data
    await sheets.spreadsheets.values.batchUpdate({
      spreadsheetId: spreadsheetId,
      requestBody: {
        data: values,
        valueInputOption: 'USER_ENTERED',
      },
    });

    console.log('Spreadsheet created and data added.', spreadsheetId);

    res.json({status : "200", message : "Spreadsheet "+spreadsheetId+" created and data added"})

  } catch (error) {
    console.error('Error:', error);
    throw new Error('Failed to create spreadsheet');
  }
}

module.exports = {
  createSpreadsheet
}
