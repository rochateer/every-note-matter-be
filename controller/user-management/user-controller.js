const { dbconn } = require("../../config/db/db.config");
const { userService } = require("../../services/user");


const getUsers = async (req, res) => {

  const query = req.query;

  const getuser = await userService.getAllUser();

  res.status(200).json({data : getuser})

}

module.exports = {
  getUsers
}