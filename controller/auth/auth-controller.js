const { userService } = require("../../services/user");

const login = async (req, res) => {

  const body = req.body;

  if(!body.email || !body.password) {
    return res.status(400).json({ errors: 'Invalid email or password' });
  }

  try{

    const getUser = await userService.getUserByEmail(body.email);

    if(!getUser || getUser.length === 0){

      const user_data = userService.createUserPreprocessing(body);

      console.log("test login user data")

      // const createUser = await userService.createUserDb(user_data);
      // const await create
    }else{

      if(getUser[0].password === body.password){
        return res.status(500).json({error : "Invalid email or password"})
      }

    }
  
    return res.status(200).json({data : 'success'});

  }catch(error){

    console.log("error", error)
    res.status(500).json({error : "Internal server error"})

  }

}

module.exports = {
  login
}